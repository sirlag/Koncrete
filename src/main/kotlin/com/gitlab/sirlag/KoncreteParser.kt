package com.gitlab.sirlag

import com.gitlab.sirlag.Exceptions.MissingRequiredArgumentException
import com.gitlab.sirlag.Config.ArgsMap
import com.gitlab.sirlag.Config.KoncreteConfig
import com.gitlab.sirlag.Config.KoncreteParameter
import com.gitlab.sirlag.Config.KoncreteParameterType

import java.util.*
import kotlin.system.exitProcess

/**
 * A singleton used to parse [KoncreteConfig]s.
 */
object KoncreteParser {

    /**
     * Parses the user input to various flags, checks for required flags, then returns the parsed flags
     *
     * @param config a [KoncreteConfig] with a list of [KoncreteParameter]s representing your expected input
     * @param args the Array of Strings that is read as part of standard input
     * @return an [ArgsMap] containing all parsed values from the user input
     */
    fun parseArgs(config: KoncreteConfig, args: Array<String>): ArgsMap<String, Any?> {
        val tokens = TokenizeArgs(args)
        val flags = tokens.map { it.split(" ").first().removePrefix("-").removePrefix("-").substringBefore("=") }

        try {
            config.getRequirements()
                    .filter { !(flags.contains(it.shortFlag) || flags.contains(it.longFlag)) }
                    .forEach { throw MissingRequiredArgumentException("Unable to find required argument ${it.name}") }

            config.parameterList
                    .filter { it.conditionallyRequired(flags) }
                    .filter { !(flags.contains(it.shortFlag) || flags.contains(it.longFlag)) }
                    .forEach { throw MissingRequiredArgumentException("Unable to find conditionally required argument ${it.name}") }
        } catch ( ex: MissingRequiredArgumentException){
            println(ex.message)
            printHelpMessages(config)
            exitProcess(1)
        }
        val parsedArgs = ArgsMap<String, Any?>()
        for(parameter in config.parameterList) {
            val parsed = {
                if (flags.contains(parameter.longFlag))
                    parseFlag(parameter, tokens[flags.indexOf(parameter.longFlag)])
                else if (flags.contains(parameter.shortFlag))
                    parseFlag(parameter, tokens[flags.indexOf(parameter.shortFlag)])
                else if (parameter.type == KoncreteParameterType.BOOLEAN)
                    false
                else
                    null
            }.invoke()
            parsedArgs.put(parameter.name, parsed)
        }

        return parsedArgs

    }

    /**
     * @param koncreteParameter The parameter to be parsed to
     * @param token A String containing the input for that flag
     */
    fun parseFlag(koncreteParameter: KoncreteParameter, token: String): Any {
        return when(koncreteParameter.type){
            KoncreteParameterType.BOOLEAN -> true
            KoncreteParameterType.STRING -> token.replace(" ", "=").substringAfter("=")
            KoncreteParameterType.DOUBLE -> try {
                token.replace(" ", "=").substringAfter("=").toDouble()
            } catch(ex: NumberFormatException){
                //TODO Print Help Message
                exitProcess(2)
            }
            KoncreteParameterType.INT -> try {
                token.replace(" ", "=").substringAfter("=").toInt()
            } catch(ex: NumberFormatException) {
                //TODO Print Help Message
                exitProcess(3)
            }
        }
    }

    /**
     * A function to turn the parameters into just the flags.
     *
     * @param args the args after being read from standard input
     * @return an [ArrayList] containing just the flags
     */
    private fun TokenizeArgs(args: Array<String>): ArrayList<String> {
        val tokens = ArrayList<String>()
        var storage = ""
        for (str in args) {
            if (str.startsWith("-") && storage != "") {
                tokens.add(storage.trim())
                storage = ""
            }
            storage += str + " "
        }
        tokens += storage
        return tokens
    }

    /**
     * Prints a help message to standard output
     *
     * @param config The [KoncreteConfig] to print out
     */
    fun printHelpMessages(config: KoncreteConfig){
        println("Usage: ${config.usage}")
        config.parameterList.forEach { println(generateHelpMessage(it)) }
    }

    /**
     * Generates a Help Message for a given [KoncreteParameter]
     * @param parameter The parameter to format
     * @return The formated help message
     */
    fun generateHelpMessage(parameter: KoncreteParameter) = "\t-${parameter.shortFlag} || --${parameter.longFlag}" +
            "\t${if(!parameter.required) "Not " else ""}Required" +
            "\t${parameter.description}"

}

/*
    Design Documentation.

    1) Tokenize the input (Find out what we have)
        Each portion of the input will be placed in a list
            That list gets placed into another list if the next token is a flag, and the process is repeated.
    2) Determine what inputs are required based on the tokenized inputs and configuration of the parser.
        (Check all *required* and *conditionally required* fields against the tokenized flags.
        Throw error if required flags are missing.
    3) Parse the tokenized flags, using the first list as a queue
        Boolean values have a default value of "false" if they are not present, true if they are.
        String, int, and double flags can be read in two different ways
            {-f||--flag}=somevalue or -f value_of_the_flag
    4) Store the outputs in a hashmap from the parser object, with dedicated access methods by type.
 */