package com.gitlab.sirlag.Exceptions

class ParameterAlreadyAddedException(msg:String? = null, cause: Throwable? = null):Exception(msg, cause)
class MissingRequiredArgumentException(msg:String? = null, cause: Throwable? = null):Exception(msg, cause)
