package com.gitlab.sirlag.Config

import com.gitlab.sirlag.Exceptions.ParameterAlreadyAddedException
import java.util.*

/**
 * A Koncrete configuration.
 *
 * This class is designed to hold the parameters that you are looking for
 *
 * @property usage the usage statement of this Koncrete configuration. By default this is empty.
 */
class KoncreteConfig(var usage: String = "") {
    val parameterList = ArrayList<KoncreteParameter>()

    /**
     * Adds a [KoncreteParameter] to this configuration.
     *
     * If a parameter already has the chosen long or short flag, throws [ParameterAlreadyAddedException]
     *
     * @param parameter the parameter to be added
     */
    fun addParameter(parameter: KoncreteParameter) {
        if(hasParameter(parameter))
            throw ParameterAlreadyAddedException("A Parameter with either the long flag {--${parameter.longFlag}}" +
                    " or short flag {-${parameter.shortFlag}} has already been added")
        parameterList += parameter
    }

    /**
     * Checks to see if a given [KoncreteParameter] is in the configuration, by checking long and short flags
     *
     * @return a boolean showing if the parameter is present
     */
    fun hasParameter(parameter: KoncreteParameter): Boolean {
        return parameterList
                .filter {it.longFlag == parameter.longFlag || it.shortFlag == parameter.shortFlag }
                .size > 0
    }

    /**
     * Searches the list of parameters for the requested parameter
     *
     * @param flag the flag of the parameter you are trying to get
     * @param longFlag if this is true, you are searching for a long flag. by default this is false
     * @return a [KoncreteParameter] from the configuration file
     */
    fun getParameter(flag: String, longFlag: Boolean = false): KoncreteParameter{
        if(longFlag)
            return parameterList.filter { it.longFlag == flag }.first()
        return parameterList.filter { it.shortFlag == flag }.first()
    }

    /**
     * Searches the list of [KoncreteParameter]s for all values marked as required
     * @return the required parameters
     */
    fun getRequirements(): List<KoncreteParameter>{
        return parameterList.filter { it.required }
    }

}

/**
 * A single parameter to be parsed by the koncrete parser.
 *
 * @param name The ID of your parameter
 * @param shortFlag the -short flag of your parameter. By default this is the first letter of your ID. if This ID is
 * starts with the same letter as another, it will need to be specified manually.
 * @param longFlag the --long-flag of your parameter. By default this is the same as your ID
 * @param type a [KoncreteParameterType], representing the type of variable you are looking for
 * @param description What your Parameter is looking for, and what it does.
 * @param required Is this parameter required for your program to run
 * @param conditionallyRequired If your parameter should be required if another flag is present (i.e. specifying
 * directories for a headless mode), they can be defined here, in a lambda that returns a boolean. by default this
 * returns false.
 */
class KoncreteParameter(val name: String,
                        val shortFlag: String = name[0].toString(),
                        val longFlag: String = name,
                        val type: KoncreteParameterType,
                        val description: String,
                        val required: Boolean = false,
                        val conditionallyRequired: (flags: List<String>) -> Boolean = {false})

/**
 * An Enum class to represent the various types that can be parsed.
 */
enum class KoncreteParameterType(val type: Class<*>){
    STRING(String::class.java),
    BOOLEAN(Boolean::class.java),
    INT(Int::class.java),
    DOUBLE(Double::class.java)
}

/**
 * An extension to [HashMap], ArgsMap adds typed get methods to assist in retrieving your parameters
 */
class ArgsMap<K, V>(): HashMap<K, V>(){
    fun getString(key: K) = this[key] as String
    fun getInt(key: K) = this[key] as Int
    fun getBoolean(key: K) = this[key] as Boolean
    fun getDouble(key: K) = this[key] as Double
}