package com.gitlab.sirlag

import com.gitlab.sirlag.Config.ArgsMap
import com.gitlab.sirlag.Config.KoncreteConfig
import com.gitlab.sirlag.Config.KoncreteParameter
import com.gitlab.sirlag.Config.KoncreteParameterType
import com.winterbe.expekt.should
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

class KoncreteParserTest : Spek({
    describe("An argument parser"){
        val koncreteConfig = KoncreteConfig()
        koncreteConfig.addParameter(KoncreteParameter("nogui", "n", "no-gui", KoncreteParameterType.BOOLEAN, "Toggles no GUI mode", false))
        koncreteConfig.addParameter(KoncreteParameter("source", "s", "source", KoncreteParameterType.STRING, "Sets source Folder", false))
        val args = "-n -s {path/to/downloads/folder} -t {path/to/music/library}".split(" ").toTypedArray()

        it("should print -n -s and -t"){
            for ((key, value) in KoncreteParser.parseArgs(koncreteConfig, args))
                println(value.toString())
        }

    }

    describe("A help message"){

        val konfig = KoncreteConfig()
        konfig.usage = "java -jar testKoncrete -t=\"Hello World\""

        val testParameter = KoncreteParameter("Test Argument", "t", "test", KoncreteParameterType.STRING, "Demonstrates the help message", true)

        konfig.addParameter(testParameter)

        it("should print out a help message"){
            val output = KoncreteParser.generateHelpMessage(testParameter)
            output.should.equal("	-t || --test	Required	Demonstrates the help message")
        }

        it("should display usage reports and the help options"){
            KoncreteParser.printHelpMessages(konfig)
        }

    }

    describe("a map of arguments"){
        val argMap = ArgsMap<String, Any?>()


    }
})